﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using WpfMvvmBitmapDemo.Models;

namespace WpfMvvmBitmapDemo.Presentation
{
    public class MainViewModel : ObservableObject
    {
        private const int maxRow = 600;
        private const int maxColumn = 800;

        private readonly ILogic logic;
        private readonly Color[] colors = new[] {Colors.Red, Colors.Blue, Colors.Green, Colors.Yellow};

        private bool working = false;
        private int count;

        public string Title => "Wpf-Mvvm-Bitmap Demo";

        public int Count
        {
            get => count;
            private set => SetProperty(ref count, value);
        }

        public string Background { get; private set; }

        public WriteableBitmap BitmapDisplay { get; private set; }

        public IRelayCommand DoWorkCommand { get; private set; }

        public IRelayCommand<string> SetColorCommand { get; private set; }

        public MainViewModel(ILogic logic)
        {
            this.logic = logic;
            SetColorCommand = new RelayCommand<string>(SetColor); 
            DoWorkCommand = new RelayCommand(async () => await DoWorkAsync(), () => !working);                        
            CreateBitmap(maxColumn,maxRow);
        }

        private void CreateBitmap(int width, int height)
        {
            double dpiX = 96d;
            double dpiY = 96d;
            var pixelFormat = PixelFormats.Pbgra32;
            BitmapDisplay = new WriteableBitmap(width, height, dpiX, dpiY, pixelFormat, null);
            OnPropertyChanged(nameof(BitmapDisplay));
        }


        private void SetPixel(int row, int column, Color color)
        {
            // set an individual pixel 
            // warning: manipulating individual pixels is very slow !!!
            // there are more efficiënt alternatives using 'unsafe' code.

            uint intColor = BitConverter.ToUInt32(new byte[] { color.B, color.G, color.R, color.A });
            uint[] pixels = new uint[] { intColor};        
            var rectangle = new Int32Rect(0, 0, 1, 1);
            BitmapDisplay.WritePixels(rectangle, pixels, BitmapDisplay.BackBufferStride,column, row);
        }

        private void SetRow(int row, Color color)
        {
            // Set an entire row of pixel to the same color
            uint[] pixels = new uint[maxColumn];
            uint intColor = BitConverter.ToUInt32(new byte[] { color.B, color.G,color.R, color.A });
            for (int i = 0; i < maxColumn; i++)
            {
                pixels[i] = intColor;
            }
            var rectangle = new Int32Rect(0, 0, maxColumn, 1);
            BitmapDisplay.WritePixels(rectangle, pixels, BitmapDisplay.BackBufferStride, 0, row);
        }

        private void SetBlock(Color[,] colors, int startRow, int startColumn)
        {
            // Set a specific block of the bitmap to the specified colors
            
            int numberOfRows = colors.GetLength(1);
            int numberOfColumns = colors.GetLength(0);
            uint[,] pixels = new uint[numberOfColumns, numberOfRows];
            for (int row = 0; row < numberOfRows; row++)
            {
                for (int column = 0; column < numberOfColumns; column++)
                {
                    var color = colors[column, row];
                    pixels[column, row] = BitConverter.ToUInt32(new byte[] { color.B, color.G, color.R, color.A });
                }
            }
            var rectangle = new Int32Rect(0, 0, numberOfColumns, numberOfRows);
            BitmapDisplay.WritePixels(rectangle, pixels, BitmapDisplay.BackBufferStride, startColumn, startRow);
        }

        private async Task DoWorkAsync()
        {
            working = true;
            DoWorkCommand.NotifyCanExecuteChanged();
            await ShowLines();
            await ShowPixels();
            Count++;
            working = false;
            DoWorkCommand.NotifyCanExecuteChanged();
        }

        private async Task ShowPixels()
        {
            var pointList = await logic.GetPointsAsync(10000);
            // show pixels, positions based on logic values, color based on Count-value
            var color = colors[Count % colors.Length];
            int rowCount = maxRow - 200;
            var pixelBlock = new Color[maxColumn,rowCount];
            foreach (var point in pointList)
            {
                int column = (int)(maxColumn * point.X);
                int row = (int)(rowCount * point.Y);
                pixelBlock[column,row] =  color;
            }
            SetBlock(pixelBlock, 200, 0);
        }

        private async Task ShowLines()
        {
            // show lines, colors based on logic values
            var rowList = await logic.GetValuesAsync(200);
            int row = 0;
            foreach (byte value in rowList)
            {
                var color = Color.FromRgb(value, value, (byte)((Count * 50) % 256));
                SetRow(row, color);
                row++;
            }
        }

        private void SetColor(string color)
        {
            Background = color switch
            {
                "Green" => "PaleGreen",
                "Blue" => "LightSkyBlue",
                _ => "Silver"
            };
            OnPropertyChanged(nameof(Background));
        }

    }
}
