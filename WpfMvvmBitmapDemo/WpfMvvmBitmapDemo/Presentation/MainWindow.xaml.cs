﻿using System.Windows;

namespace WpfMvvmBitmapDemo.Presentation
{
   
    public partial class MainWindow : Window
    {
        public MainWindow(MainViewModel vm)
        {
            DataContext = vm;
            InitializeComponent();
        }

        private void WindowPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Space) return;            
            var viewModel = DataContext as MainViewModel;
            if (viewModel.DoWorkCommand.CanExecute(null)) viewModel.DoWorkCommand.Execute(null);
        }
    }
}
