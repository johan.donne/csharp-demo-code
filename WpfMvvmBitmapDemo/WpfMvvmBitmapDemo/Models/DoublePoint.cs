﻿namespace WpfMvvmBitmapDemo.Models
{
    public record DoublePoint(double X, double Y);
}
