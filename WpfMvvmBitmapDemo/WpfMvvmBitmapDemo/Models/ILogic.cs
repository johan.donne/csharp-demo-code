﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfMvvmBitmapDemo.Models
{
    public interface ILogic
    {
        Task<List<byte>> GetValuesAsync(int count);
        Task<List<DoublePoint>> GetPointsAsync(int count);
    }
}
