﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfButtons
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // create brush from RGB value
        private readonly Brush greenBrush = new SolidColorBrush(Color.FromRgb(50, 200, 50));
        private readonly Brush redBrush = new SolidColorBrush(Color.FromRgb(200, 50, 50));

        private readonly List<Button> buttonList = new List<Button>();

        //use predefined SolidColorBrushes
        private readonly List<Brush> colorList = new List<Brush>()
        {
            Brushes.Aquamarine,
            Brushes.Yellow,
            Brushes.BlanchedAlmond,
            Brushes.LightGray
        };


        public MainWindow()
        {
            InitializeComponent();
            CreateBottomButtons();
        }

        private void TopButtonClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            int column = Grid.GetColumn(button);
            colorPanel.Background = column == 0 ? greenBrush : redBrush;
        }

        private void CreateBottomButtons()
        {
            for (int column = 0; column < 4; column++)
            {
                var button = new Button
                {
                    Content = $"Knop {column}",                    
                    FontSize = 30
                };                
                button.Click += BottomButtonClick;
                Grid.SetRow(button, 1);
                Grid.SetColumn(button, column);
                buttonList.Add(button);
                ButtonGrid.Children.Add(button);
            }
            // replace text content for third button by an image
            buttonList[2].Content = new Image()
            {
                Source = (ImageSource)new ImageSourceConverter().ConvertFrom(Properties.Resources.star)
            };

            // replace text content for last button by composite content 
            var buttonContent = new StackPanel() { Orientation = Orientation.Horizontal };
            var buttonText = new TextBlock { Text = "knop 3" };
            buttonContent.Children.Add(buttonText);
            buttonContent.Children.Add(new Image()
            {
                Source = (ImageSource)new ImageSourceConverter().ConvertFrom(Properties.Resources.star)
            });
            buttonList[3].Content = buttonContent;
        }

        private void BottomButtonClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            colorTextBox.Background = colorList[buttonList.IndexOf(button)];
        }
    }
}
