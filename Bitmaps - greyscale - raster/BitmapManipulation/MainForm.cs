﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BitmapManipulation
{
    public partial class MainForm:Form
    {
        private const int rastersize = 150;  

        private Bitmap original;
        private Bitmap processed;
        private string imagePath;
        private readonly Color rastercolor = Color.White;

        public MainForm()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.favicon;
        }

        private void LoadImage(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                if (!string.IsNullOrWhiteSpace(imagePath)) dialog.InitialDirectory = imagePath;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    imagePath = Path.GetDirectoryName(dialog.FileName);
                    original?.Dispose();
                    original = new Bitmap(Image.FromStream(dialog.OpenFile()));
                    originalPictureBox.Image = original;
                    processed?.Dispose();
                    processed = new Bitmap(original);
                    processedPictureBox.Image = processed;
                    grayCheckBox.Checked = false;
                    rasterCheckBox.Checked = false;
                }
            }
        }

        private void AdjustImage()
        {
            processed?.Dispose();
            processed = new Bitmap(original);
            GrayScale();
            SetRaster();
            processedPictureBox.Image = processed;
        }

        private void GrayScale()
        {
            if (!grayCheckBox.Checked) return;
            {
                using (var graphics = Graphics.FromImage(processed))
                {
                    Pen blackPen = new Pen(Color.Yellow,20);

                    graphics.DrawLine(blackPen, 0, 0, processed.Width, processed.Height);
                }
            }
        }

        private static Color GetGray(Color color)
        {
            byte gray =  (byte)(0.2126 * color.R + 0.7152 * color.G + 0.0722 * color.B);
            return Color.FromArgb(gray, gray, gray);
        }

        private void SetRaster()
        {
            if(!rasterCheckBox.Checked) return;
            for(int row = 0; row < processed.Height; row++)
            {
                for(int column = 0; column < processed.Width; column++)
                {
                    if((row % rastersize != 0) && (column % rastersize != 0)) continue;
                    var color = processed.GetPixel(column, row);
                    color = Color.FromArgb
                        (
                            (byte) (color.R ^ rastercolor.R),
                            (byte)(color.G ^ rastercolor.G),
                            (byte)(color.B ^ rastercolor.B)
                        );
                    processed.SetPixel(column, row, color);
                }
            }
        }
               
        private void StatusChanged(object sender, EventArgs e)
        {
            statusLabel.Text = "Aan het rekenen...";
            statusLabel.Refresh();
            AdjustImage();
            statusLabel.Text = string.Empty;
        }

        private void OnMainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            original?.Dispose();
            processed?.Dispose();
        }
    }
}
