﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveTree
{
    public class TreeInfo
    {

        public int Depth { get; private set; }
        public int NrOfDirectories { get; private set; }
        public int NrOfFiles { get; private set; }
        public int NrOfExtensions
        {
            get
            {
                return Files.Count();
            }
        }

        public Dictionary<string, List<FileInfo>> Files { get; private set; }

        public TreeInfo(string path)
        {
            if(!Directory.Exists(path)) throw new ArgumentException($" \'{path}\' is geen directory");
            InitializeProperties();
            PopulateFiles(path, 1);
        }

        private void InitializeProperties()
        {
            Files = new Dictionary<string, List<FileInfo>>();
            Depth = 0;
            NrOfDirectories = 0;
            NrOfFiles = 0;
        }

        private void PopulateFiles(string path, int depth)
        {
            if(depth > Depth) Depth = depth;
            try
            {
                ProcessFiles(path);
                ProcessDirectories(path, depth);
            }
            catch(Exception)
            {
                // do nothing, no access                
            }

        }

        private void ProcessFiles(string path)
        {

            foreach(string file in Directory.GetFiles(path))
            {
                try
                {
                    var info = new FileInfo(file);
                    string extension = info.Extension.ToLower();
                    if(!Files.ContainsKey(extension)) Files.Add(extension, new List<FileInfo>());
                    Files[extension].Add(info);
                    NrOfFiles++;
                }
                catch(Exception )
                {
                    // do nothing, ignore files with no access      
                }
            }
        }

        private void ProcessDirectories(string path, int depth)
        {
            foreach(string directory in Directory.GetDirectories(path))
            {
                try
                {
                    PopulateFiles(directory, depth + 1);
                    NrOfDirectories++;
                }
                catch(Exception)
                {
                    // do nothing, ignore files with no access      
                }
            }
        }

        public List<ExtensionItem> GetTopFiveExtensions()
        {
            var result = new List<ExtensionItem>();
            var sorter = new List<ExtensionItem>();
            foreach(string extension in Files.Keys)
            {
                sorter.Add(new ExtensionItem(extension, Files[extension].Count));
            }
            sorter.Sort();
            sorter.Reverse();
            for(int i = 0; i < 5; i++)
            {
                if(i >= sorter.Count) break;
                result.Add(sorter[i]);
            }
            return result;
        }

    }
}
