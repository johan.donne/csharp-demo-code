﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveTree
{
    public static class ConsoleUi
    {
        
        public static void ShowStartBanner()
        {
            Console.WriteLine("TreeInfo\n");
        }

        public static string GetPath()
        {
            Console.Write("Geef een directory pad in:");
            string path = Console.ReadLine();
            Console.WriteLine();
            return path;
        }

        public static void RunTreeInfo(string path)
        {
            try
            {
                var info = new TreeInfo(path);
                WriteTreeProperties(info);
                WriteTopExtensions(info);
            }
            catch(ArgumentException e)
            {
                Console.WriteLine($"\n{e.Message}\n");
            }
        }

        private static void WriteTreeProperties(TreeInfo info)
        {
            Console.WriteLine($"Aantal mappen-niveau's: {info.Depth}");
            Console.WriteLine($"Aantal mappen: {info.NrOfDirectories}");
            Console.WriteLine($"Aantal bestanden: {info.NrOfFiles}");
            Console.WriteLine($"Aantal extensies: {info.NrOfExtensions}");
        }

        private static void WriteTopExtensions(TreeInfo info)
        {
            Console.WriteLine($"Top extensies:\n");
            var top = info.GetTopFiveExtensions();
            foreach(var extInfo  in top)
            {
                Console.WriteLine($"   {extInfo.Extension,-10}:  {extInfo.Count} bestanden");
            }
        }
        
        public static void ShowEndBanner()
        {
            Console.WriteLine("\n\n<Enter> to continue\n");
            Console.ReadLine();
        }
    }
}
