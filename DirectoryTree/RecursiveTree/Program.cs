﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveTree
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleUi.ShowStartBanner();
            string path = (args.Length == 0) ? ConsoleUi.GetPath() : args[0];
            ConsoleUi.RunTreeInfo(path);
            ConsoleUi.ShowEndBanner();
        }
    }
}
