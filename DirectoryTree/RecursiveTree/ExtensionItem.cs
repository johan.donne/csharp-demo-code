﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursiveTree
{
    public class ExtensionItem: IComparable<ExtensionItem>
    {
        public string Extension { get; private set; }
        public int Count { get; private set; }

        public ExtensionItem(string extension, int count)
        {
            this.Extension = extension;
            this.Count = count;
        }

        public int CompareTo(ExtensionItem other)
        {
            return this.Count.CompareTo(other.Count);
        }
    }
}
