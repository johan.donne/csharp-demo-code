﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecursiveTree;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class ApplicationTests
    {
        [TestMethod, Timeout(100)]
        public void MethodDeclarationsOK()
        {
            var x = typeof(ConsoleUi);
            Helper.CheckMethod(x, "ShowStartBanner", new Type[] { typeof(void) }, Array.Empty<Type>());
            Helper.CheckMethod(x, "ShowEndBanner", new Type[] { typeof(void) }, Array.Empty<Type>());
            Helper.CheckMethod(x, "GetPath", new Type[] { typeof(string), typeof(String) }, Array.Empty<Type>());
            Helper.CheckMethod(x, "RunTreeInfo", new Type[] { typeof(void) }, new Type[] { typeof(string) } );
        }

        [TestMethod, Timeout(1000)]
        public void GetPathOk()
        {
            string consoleInput = @"c:\test";
            string path;
            using(var sw = new StringWriter())
            {
                using(var sr = new StringReader($"{consoleInput}\n\n"))
                {
                    Console.SetOut(sw);
                    Console.SetIn(sr);
                    path = ConsoleUi.GetPath();
                }
            }
            Assert.IsTrue(path == consoleInput,$"ConsoleUi.GetPath returns \'{path}\' when \'{consoleInput}\' was entered.");
        }

        [TestMethod, Timeout(10000)]
        public void RunTreeInfoBasicOk()
        {
            const string path = ".";
            var sb = new StringBuilder();
            using(var sw = new StringWriter(sb))
            {
                using(var sr = new StringReader($"\n\n\n"))
                {
                    Console.SetOut(sw);
                    Console.SetIn(sr);
                    ConsoleUi.RunTreeInfo(path);
                    sw.Flush();
                }
            }
            var info = new TreeInfo(path);
            string result = sb.ToString();
            Assert.IsTrue(result.Contains(info.Depth.ToString()), $"ConsoleUi.RunTreeInfo does not show Depth value.");
            Assert.IsTrue(result.Contains(info.NrOfDirectories.ToString()), $"ConsoleUi.RunTreeInfo does not show NrOfDirectories value.");
            Assert.IsTrue(result.Contains(info.NrOfFiles.ToString()), $"ConsoleUi.RunTreeInfo does not show NrOfFiles value.");
            Assert.IsTrue(result.Contains(info.NrOfExtensions.ToString()), $"ConsoleUi.RunTreeInfo does not show NrOfExtensions value.");
        }

        [TestMethod, Timeout(10000)]
        public void RunTreeInfoTopOk()
        {
            const string path = "..";
            var sb = new StringBuilder();
            using(var sw = new StringWriter(sb))
            {
                using(var sr = new StringReader($"\n\n\n"))
                {
                    Console.SetOut(sw);
                    Console.SetIn(sr);
                    ConsoleUi.RunTreeInfo(path);
                    sw.Flush();
                }
            }
            string result = sb.ToString();
            var top = new TreeInfo(path).GetTopFiveExtensions();
            foreach(var item in top)
            {
                Assert.IsTrue(result.Contains(item.Extension), $"ConsoleUi.RunTreeInfo does not show extension {item.Extension}.");
                Assert.IsTrue(result.Contains(item.Count.ToString()), $"ConsoleUi.RunTreeInfo does not show count for {item.Extension} extension.");
            }           
        }

        [TestMethod, Timeout(10000)]
        public void FullApplicationNoArgOk()
        {
            const string path = "..";
            const string executable = "opgave02.exe";
            Assert.IsTrue(File.Exists(executable),"\'Opgave02.exe\' is niet gevonden.");
            string output;
            using(var p = new Process())
            {
                p.StartInfo.FileName = executable;
                p.StartInfo.Arguments = string.Empty;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardInput = true;
                p.Start();
                p.StandardInput.WriteLine($"{path}\n\n\n");
                output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            var info = new TreeInfo(path);
            Assert.IsTrue(output.Contains(info.Depth.ToString()), $"Running the application does not show Depth value.");
            Assert.IsTrue(output.Contains(info.NrOfDirectories.ToString()), $"Running the application does not show NrOfDirectories value.");            
        }


        [TestMethod, Timeout(10000)]
        public void FullApplicationWithArgOk()
        {
            const string path = "..";
            const string executable = "opgave02.exe";
            Assert.IsTrue(File.Exists(executable), "\'Opgave02.exe\' is niet gevonden.");
            string output;
            using(var p = new Process())
            {
                p.StartInfo.FileName = executable;
                p.StartInfo.Arguments = path;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardInput = true;
                p.Start();
                p.StandardInput.WriteLine($"\n\n\n");
                output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            var info = new TreeInfo(path);
            Assert.IsTrue(output.Contains(info.Depth.ToString()), $"Running the application does not show Depth value.");
            Assert.IsTrue(output.Contains(info.NrOfDirectories.ToString()), $"Running the application does not show NrOfDirectories value.");
        }

        [TestMethod, Timeout(10000)]
        public void ApplicationWaitsForEnter()
        {
            const string path = ".";
            const string executable = "opgave02.exe";
            Assert.IsTrue(File.Exists(executable), "\'Opgave02.exe\' is niet gevonden.");
            string output;
            var start = DateTime.Now;
            var job = Task.Run(() =>
           {
               using(var p = new Process())
               {
                   p.StartInfo.FileName = executable;
                   p.StartInfo.Arguments = path;
                   p.StartInfo.UseShellExecute = false;
                   p.StartInfo.RedirectStandardOutput = true;
                   p.StartInfo.RedirectStandardInput = true;
                   p.Start();
                   output = p.StandardOutput.ReadToEnd();
                   p.WaitForExit();
               }
           });
           job.Wait(2000);
           Assert.IsTrue((DateTime.Now-start)>TimeSpan.FromMilliseconds(1900), $"The application closes without waiting for Enter key.");
        }

    }
}
