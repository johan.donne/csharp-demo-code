﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecursiveTree;
using System;
using System.Linq;

namespace UnitTests
{
    internal class Helper
    {
        public static Type GetTypeByName(string typeName)
        {
            var foundClass = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                              from type in assembly.GetTypes()
                              where type.Name == typeName
                              select type).FirstOrDefault();
            return foundClass;
        }
        public static void CheckProperty(Type t, string propName, Type[] propTypes, bool hasPublicGetter, bool hasPublicSetter, bool hasPrivateGetter, bool hasPrivateSetter)
        {
            var props = t.GetProperties();
            var prop = props.Where(p => p.Name == propName).FirstOrDefault();
            Assert.IsNotNull(prop, $"Type {t.GetType().Name} has no public \'{propName}\' property");
            Assert.IsTrue(Array.Exists(propTypes, p => p.Name == prop.PropertyType.Name),
                              $"{typeof(object).Name}: property \'{propName}\' is a {prop.PropertyType.Name}");
            if (hasPrivateGetter || hasPublicGetter)
            {
                Assert.IsNotNull(prop.GetMethod, $"{t.Name}: property {propName} has no Getter ");
                Assert.IsTrue(prop.GetMethod.IsPublic == hasPublicGetter, $"{t.Name}: property {propName} has {(prop.GetMethod.IsPublic ? "a" : "no")} public Getter ");
                Assert.IsTrue(prop.GetMethod.IsPrivate == hasPrivateGetter, $"{t.Name}: property {propName} has {(prop.GetMethod.IsPrivate ? "a" : "no")} private Getter ");
            }
            else
            {
                Assert.IsNull(prop.GetMethod, $"{t.Name}: property {propName} has a Getter (not allowed)");
            }
            if (hasPrivateSetter || hasPublicSetter)
            {
                Assert.IsNotNull(prop.SetMethod, $"{t.Name}: property {propName} has no Setter ");
                Assert.IsTrue(prop.SetMethod.IsPublic == hasPublicSetter, $"GlobalTests - {t.Name}: property {propName} has {(prop.SetMethod.IsPublic ? "a" : "no")} public Setter ");
                Assert.IsTrue(prop.SetMethod.IsPrivate == hasPrivateSetter, $"GlobalTests - {t.Name}: property {propName} has {(prop.GetMethod.IsPrivate ? "a" : "no")} private Setter ");
            }
            else
            {
                Assert.IsNull(prop.SetMethod, $"{t.Name}: property {propName} has a Setter (not allowed)");
            }
        }
        public static void CheckMethod(Type t, string methodName, Type[] returnTypes, Type[] parameterTypes)
        {
            var methods = t.GetMethods();
            // check if method exists with right signature
            var method = methods.Where(m =>
            {
                if (m.Name != methodName) return false;
                var parameters = m.GetParameters();
                if (parameterTypes == null || parameterTypes.Length == 0) return parameters.Length == 0;
                if (parameters.Length != parameterTypes.Length) return false;
                for (int i = 0; i < parameterTypes.Length; i++)
                {
                    // if (parameters[i].ParameterType != parameterTypes[i])
                    if (!parameters[i].ParameterType.IsAssignableFrom(parameterTypes[i]))
                        return false;
                }
                return true;
            }).FirstOrDefault();
            Assert.IsNotNull(method, $"Type {t.FullName} has no public \'{methodName}\' method with the right signature");

            // check returnType
            Assert.IsTrue(Array.Exists(returnTypes, r => r.Name == method.ReturnType.Name),
                              $"Type {typeof(object).Name}: method \'{methodName}\' returns a \'{method.ReturnType.Name}\'");
        }
    }


    [TestClass]
    public class ExtensionItemTests
    {
        [TestMethod, Timeout(100)]
        public void HasRightProperties()
        {
            var x = typeof(ExtensionItem);
            Helper.CheckProperty(x, "Extension", new Type[] { typeof(string) }, true, false, false, true);
            Helper.CheckProperty(x, "Count", new Type[] { typeof(int), typeof(Int16) }, true, false, false, true);
        }

        [TestMethod, Timeout(100)]
        public void HasRightConstructors()
        {
            var x = typeof(ExtensionItem);
            var constructor = x.GetConstructor(Array.Empty<Type>());
            Assert.IsNull(constructor,
                $"\'ExtensionItem\' has a default constructor (not allowed)!");
            constructor = x.GetConstructor(new Type[] { typeof(string), typeof(int) });
            Assert.IsNotNull(constructor,
                $"\'ExtensionItem\' does not contain a constructor with parameters of type \'string\' & \'int\'.");
        }

        [TestMethod, Timeout(100)]
        public void CompareToOk()
        {
            var a = new ExtensionItem("A", 1);
            var b = new ExtensionItem("B", 1);
            Assert.IsTrue(a.CompareTo(b) == 0, $"\'ExtensionItem.CompareTo' does not return 0 for equal lengths.");
            b = new ExtensionItem("B", 2);
            Assert.IsTrue(a.CompareTo(b) == 1.CompareTo(2), $"\'ExtensionItem.CompareTo' returns wrong value.");
            Assert.IsTrue(b.CompareTo(a) == 2.CompareTo(1), $"\'ExtensionItem.CompareTo' returns wrong value.");
        }

        [TestMethod, Timeout(100)]
        public void InstantiatesOk()
        {
            var a = new ExtensionItem("A", 1);
            Assert.IsTrue(a.Extension == "A", $"\'ExtensionItem.Extension\' does not return extension passed in the constructor.");
            Assert.IsTrue(a.Count == 1, $"\'ExtensionItem.Extension\' does not return count passed in the constructor.");
        }
    }



}
