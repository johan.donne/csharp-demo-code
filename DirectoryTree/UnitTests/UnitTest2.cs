﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecursiveTree;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace UnitTests
{
    [TestClass]
    public class TreeInfoTests
    {
        [TestMethod, Timeout(100)]
        public void HasRightProperties()
        {
            var x = typeof(TreeInfo);
            Helper.CheckProperty(x, "Depth", new Type[] { typeof(int), typeof(Int16) }, true, false, false, true);
            Helper.CheckProperty(x, "NrOfDirectories", new Type[] { typeof(int), typeof(Int16) }, true, false, false, true);
            Helper.CheckProperty(x, "NrOfFiles", new Type[] { typeof(int), typeof(Int16) }, true, false, false, true);
            Helper.CheckProperty(x, "NrOfExtensions", new Type[] { typeof(int), typeof(Int16) }, true, false, false, false);
            Helper.CheckProperty(x, "Files", new Type[] { typeof(Dictionary<string, List<FileInfo>>) }, true, false, false, true);
        }

        [TestMethod, Timeout(100)]
        public void HasRightConstructors()
        {
            var x = typeof(TreeInfo);
            var constructor = x.GetConstructor(Array.Empty<Type>());
            Assert.IsNull(constructor,
                $"\'TreeInfo\' has a default constructor (not allowed)!");
            constructor = x.GetConstructor(new Type[] { typeof(string) });
            Assert.IsNotNull(constructor,
                $"\'TreeInfo\' does not contain a constructor with parameter of type \'string\'.");
        }

        [TestMethod, Timeout(1000)]
        public void CalculatesDepthOk()
        {
            int expectedDepth = 1;
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            Assert.IsTrue(info.Depth == expectedDepth, $" TreeInfo for executing directory returns Dept = {info.Depth}, {expectedDepth} expected.");
            info = new TreeInfo("..");
            expectedDepth++;
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for \'..\' directory.");
            Assert.IsTrue(info.Depth == expectedDepth, $" TreeInfo for \'..\' directory returns Dept = {info.Depth}, {expectedDepth} expected.");
        }

        [TestMethod, Timeout(1000)]
        public void CalculatesDirectoriesOk()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            Assert.IsTrue(info.NrOfDirectories == 0, $" TreeInfo for executing directory returns NrOfDirectories = {info.NrOfDirectories}, 0 expected.");
            info = new TreeInfo("..");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for \'..\' directory.");
            Assert.IsTrue(info.NrOfDirectories != 0, $" TreeInfo for \'..\' directory returns NrOfDirectories = 0 (is impossible).");
        }

        [TestMethod, Timeout(1000)]
        public void CalculatesFilesOk()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            Assert.IsTrue(info.NrOfFiles != 0, $" TreeInfo for executing directory returns NrOfFiles  = 0 (is impossible).");
            int rootFiles = info.NrOfFiles;
            info = new TreeInfo(@"..\..\..");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for \'..\' directory.");
            Assert.IsTrue(info.NrOfFiles != rootFiles, $" TreeInfo for \'..\' directory always returns NrOfFiles = {rootFiles}");
            int totalFiles = info.Files.Values.Select((l) => l.Count).Sum();
            Assert.IsTrue(info.NrOfFiles == totalFiles, $" TreeInfo \'NrOfFiles\' = {info.NrOfFiles}, but \'Files \' contains a total of {totalFiles} Files.");

        }

        [TestMethod, Timeout(1000)]
        public void PopulatesExtensionsCorrect()
        {
            var extensions = new List<string> { ".exe", ".dll", ".pdb", ".config" };
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            foreach(string extension in extensions)
            {
                Assert.IsTrue(info.Files.ContainsKey(extension.ToLower()), $" TreeInfo for executing directory does not contain \'{extension}\' extensie.");
            }
        }

        [TestMethod, Timeout(1000)]
        public void ReturnsCorrectNrOfExtensions()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            Assert.IsTrue(info.NrOfExtensions == info.Files.Count, $" TreeInfo returns \'nrOfExtensions\' = {info.NrOfExtensions} but \'Files\' contains {info.Files.Count} extensions.");
        }

        [TestMethod, Timeout(1000)]
        public void ThrowsExceptionforInvalidPath()
        {
            Assert.ThrowsException<ArgumentException>(() => new TreeInfo("*"), $"Instantiating TreeInfo with an invalid path does not throw an ArgumentException.");
        }

        [TestMethod, Timeout(10000)]
        public void SkipsProtectedDirectories()
        {
            try
            {
                var x = new TreeInfo(@"c:\$recycle.bin");
            }
            catch(Exception)
            {
                Assert.Fail("TreeInfo does not handle exceptions for accessviolations.");
            }
        }

        [TestMethod, Timeout(1000)]
        public void TopFiveIsPlausible()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            var top = info.GetTopFiveExtensions();
            Assert.IsNotNull(top, "GetTopFiveExtensions returns null.");
            foreach(var item in top)
            {
                Assert.IsTrue(info.Files[item.Extension].Count == item.Count, $"Result of GetTopFiveExtensions are not possible for the given TreeInfo.");
            }
        }

        [TestMethod, Timeout(1000)]
        public void TopFiveOrderedOk()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            var top = info.GetTopFiveExtensions();
            Assert.IsNotNull(top, "GetTopFiveExtensions returns null.");
            for(int i = 1; i < top.Count; i++)
            {
                Assert.IsTrue(top[i].Count <= top[i - 1].Count, $"Result of GetTopFiveExtensions is not ordered descending.");
            }
        }

        [TestMethod, Timeout(1000)]
        public void TopFiveMaxOk()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            var top = info.GetTopFiveExtensions();
            Assert.IsNotNull(top, "GetTopFiveExtensions returns null.");
            int max = info.Files.Max((kvp) => kvp.Value.Count);
            Assert.IsTrue(top[0].Count == max, "GetTopFiveExtensions returns top count as {top[0].Count}, should be {max}.");
        }

        [TestMethod, Timeout(1000)]
        public void TopFiveHandlesFewExtensionsOk()
        {
            var info = new TreeInfo(".");
            Assert.IsNotNull(info, "Could not instantiate TreeInfo for current directory.");
            while(info.Files.Count > 3) info.Files.Remove(info.Files.Keys.Last());
            var top = info.GetTopFiveExtensions();
            Assert.IsNotNull(top, "GetTopFiveExtensions returns null.");
            Assert.IsTrue(top.Count == 3, "GetTopFiveExtensions returns top count as {top[0].Count}, should be {max}.");
        }
    }
}
