﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinderEngine
{
    public class PathFinder : IPathFinder
    {
        private List<List<string>> pathsFound;

        private readonly Dictionary<string, List<string>> graph;

        public ReadOnlyDictionary<string, List<string>> Graph => new ReadOnlyDictionary<string, List<string>>(graph);


        #region Construction
        public PathFinder(string[] connections)
        {
            graph=new Dictionary<string, List<string>>();
            foreach (string line in connections)
            {
                if (!string.IsNullOrEmpty(line.Trim())) ProcessGraphLine(line);                
            }
        }
               
        private void ProcessGraphLine(string line)
        {
            string nodeName = GetNodeName(line);
            var neighbours = GetNeighbours(line);
            AddNode(nodeName, neighbours);
        }

        private static string GetNodeName(string line)
        {
            string result = line.Split('-')[0].Trim().ToUpper();
            if (result.Contains(' ')) throw new ArgumentException();
            return result;
        }

        private static List<string> GetNeighbours(string line)
        {
            var neighbours = new List<string>();
            string[] neighbourStrings = line.Split('-');
            if (neighbourStrings.Length>1)
            {
                string[] neighbourArray = neighbourStrings[1].Trim().ToUpper().Split(',');
                foreach (string candidate in neighbourArray)
                {
                    if (candidate.Trim().Contains(' ')) throw new ArgumentException();
                    neighbours.Add(candidate.Trim());
                }
            }
            return neighbours;
        }

        private void AddNode(string name, List<String> neighbours)
        {
            CreateNodesIfNecessary(name, neighbours);
            AddConnections(name, neighbours);
        }

        private void AddConnections(string name, List<string> neighbours)
        {
            foreach (string neighbour in neighbours)
            {
                if (!graph[name].Contains(neighbour)) graph[name].Add(neighbour);
                if (!graph[neighbour].Contains(name)) graph[neighbour].Add(name);
            }
        }

        private void CreateNodesIfNecessary(string name, List<String> neighbours)
        {
            if (!graph.ContainsKey(name))
            {
                graph.Add(name, new List<string>());
            }
            foreach (string neighbour in neighbours)
            {
                if (!graph.ContainsKey(neighbour))
                {
                    graph.Add(neighbour, new List<string>());
                }
            }
        }
        

        #endregion Construction

        #region PathFinding
                
        public List<List<string>> GetPaths(string start, string destination)
        {
            pathsFound=new List<List<string>>();
            if (graph.ContainsKey(start) && graph.ContainsKey(destination))
            {
                // get all paths recursively
                var path = new List<string>() { start };
                FindPaths(path, destination);
            }
            return pathsFound;
        }

        private void FindPaths(List<string> currentPath, string destination)
        {
            if (currentPath.Last()==destination)
            {
                RegisterPath(currentPath);
            }
            else
            {
                ExploreNewNodes(currentPath, destination);
            }
        }

        private void ExploreNewNodes(List<string> currentPath, string destination)
        {
            var neighbours = graph[currentPath.Last()];
            foreach (string neighbour in neighbours)
            {
                if (!currentPath.Contains(neighbour))
                {
                    currentPath.Add(neighbour);
                    FindPaths(currentPath, destination);
                    currentPath.Remove(neighbour);
                }
            }
        }

        private void RegisterPath(List<string> path)
        {
            pathsFound.Add(new List<string>(path));
        }
               
        #endregion PathFinding
    }
}
