﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinderEngine
{
    public interface IPathFinder
    {
        ReadOnlyDictionary<string, List<string>> Graph { get; }

        List<List<string>> GetPaths(string start, string destination);
    }
}
