﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinderConsole
{
    class Program
    {
        static void Main()
        {
            IConsoleUi ui = new ConsoleUi();
            ui.ShowStartBanner();
            ui.ReadGraph();
            ui.ProcessQueries();
            ui.ShowEndBanner();
        }
    }
}
