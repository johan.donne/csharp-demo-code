﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinderConsole
{
    public interface IConsoleUi
    {
        void ShowStartBanner();
        void ShowEndBanner();
        void ReadGraph();
        void ProcessQueries();
    }
}
