﻿using PathFinderEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinderConsole
{
    public class ConsoleUi : IConsoleUi
    {
        private IPathFinder pathFinder;

        public void ShowStartBanner()
        {
            Console.WriteLine("PathFinder");
            Console.WriteLine("==========\n");
        }

        public void ShowEndBanner()
        {
            Console.WriteLine("\nPress <enter> to finish.");
            Console.ReadLine();
        }


        public void ReadGraph()
        {
            const string filename = @"resources\Terrain.txt";
            string[] lines = System.IO.File.ReadAllLines(filename);
            ShowLines(lines);
            pathFinder=new PathFinder(lines);
        }

        private void ShowLines(string[] lines)
        {
            Console.WriteLine("Graph connections:\n");
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
            Console.WriteLine("\n------------------\n");
        }
        
        #region ProcessQueries

        public void ProcessQueries()
        {
            string line;
            Console.Write("Enter start & destination as <start>-<destination>  (<enter> to finish):\n");
            do
            {
                Console.Write("> ");
                line=Console.ReadLine();
                ProcessLineChecked(line);
            }
            while (!string.IsNullOrEmpty(line));
        }

        private void ProcessLineChecked(string line)
        {
            if (!string.IsNullOrEmpty(line))
            {
                try
                {
                    ProcessQueryLine(line);
                }
                catch
                {
                    Console.WriteLine("Invalid Query");
                }
            }
        }

        private void ProcessQueryLine(string line)
        {
            string[] nodes = line.ToUpper().Split('-');
            if (nodes.Length!=2) throw new ArgumentException();
            string start = nodes[0].Trim();
            string destination = nodes[1].Trim();
            ProcessQuery(start, destination);
        }

        private void ProcessQuery(string start, string destination)
        {
            ShowQueryHeader(start, destination);
            var pathList = pathFinder.GetPaths(start, destination);
            Console.WriteLine($"Number of Paths found: {pathList.Count}");
            Console.WriteLine();
            foreach (var path in pathList)
            {
                ShowPath(path);
            }
            Console.WriteLine();
        }

        private static void ShowQueryHeader(string start, string destination)
        {
            string header = $"Searching path from {start} to {destination}";
            Console.WriteLine();
            Console.WriteLine(header);
            Console.WriteLine(new String('-', header.Length));
            Console.WriteLine();
        }

        private static void ShowPath(List<string> path)
        {
            for (int i = 0; i<path.Count; i++)
            {
                if (i!=0) Console.Write(" - ");
                Console.Write(path[i]);
            }
            Console.WriteLine();
        }

        #endregion ProcessQueries
    }
}
