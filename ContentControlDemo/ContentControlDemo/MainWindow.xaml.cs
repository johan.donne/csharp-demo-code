﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ContentControlDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {
        private Dictionary<string, UserControl> pages;
        public MainWindow()
        {
            InitializeComponent();
            selectionBox.SelectionChanged += OnPageSelected;
            InitPages();            
        }

        private void OnPageSelected(object sender, SelectionChangedEventArgs e)
        {
            mainPage.Content = pages[selectionBox.SelectedValue as string];
        }

        private void InitPages()
        {
            pages = new Dictionary<string, UserControl>
            {
                { "eerste", new UserControl1() },
                { "tweede", new UserControl2() }
            };
            selectionBox.ItemsSource = pages.Keys;
            //selectionBox.DisplayMemberPath = "Key";
            selectionBox.SelectedIndex = 0;
        }
    }
}
