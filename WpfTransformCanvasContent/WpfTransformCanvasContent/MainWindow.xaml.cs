﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTransformCanvasContent
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private Polygon p;
        private double scale = 1;
        private double rotation =0;
        private double translateX = 0;
        private double translateY = 0;

        public event PropertyChangedEventHandler PropertyChanged;

        // properties for user interaction
        // If any of the transform parameters are changed by the user,
        // a new Transform matrix is built and assigned to the polyline for rendering.

        public double Scale
        {
            get => scale;
            set
            {
                if (value < 0.5) return;
                scale = value;
                p.RenderTransform = BuildTransform(Rotation, Scale, TranslateX, TranslateY);
            }
        }

        public double Rotation
        {
            get => rotation;
            set
            {
                rotation = value;
                p.RenderTransform = BuildTransform(Rotation, Scale, TranslateX, TranslateY);
                // and notify the winow the rotationText should be refreshed
                OnPropertyChanged(nameof(RotationText));
            }
        }
        public string RotationText => $"{Rotation:F0}";

        public double TranslateX
        {
            get => translateX;
            set
            {
                translateX = value;
                p.RenderTransform = BuildTransform(Rotation, Scale, TranslateX, TranslateY);
            }
        }

        public double TranslateY
        {
            get => translateY;
            set
            {
                translateY = value;
                p.RenderTransform = BuildTransform(Rotation, Scale, TranslateX, TranslateY);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            // set the Datacontext for this window to itself (for Binding to properties)
            this.DataContext = this;
            p = GetPolygon();
            canvas.Children.Add(p);
        }

        private Polygon GetPolygon()
        {
            return new Polygon
            {
                Stroke = Brushes.Green,
                StrokeThickness = 2,
                Points = new PointCollection
                {
                    new Point(-10,10),
                    new Point(-10,-10),
                    new Point(10,-10),
                    new Point(10,10),
                    new Point(-10,10)
                }
            };
        }

        protected void OnPropertyChanged(string propertyName)
        {
            // notifies the window that a binding should be updated
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnCanvasSizeChanged (object sender, RoutedEventArgs e)
        {
            // To put the drawing in the middle of the canvas we need to know its size
            // and that is only known afer the Window has rendered/resized
            // so we do this in an eventhandler called when the window/canvas size has changed
            
            CenterPolygonInCanvas(p);
        }
                
        private void CenterPolygonInCanvas(Polygon p)
        {   
            Canvas.SetLeft(p, canvas.ActualWidth / 2);
            Canvas.SetTop(p, canvas.ActualHeight / 2);
        }

        private TransformGroup BuildTransform(double rotation, double scale, double translateX, double translateY)
        {
            // setup transforms
            var rotationTransform = new RotateTransform
            {
                Angle = rotation,
                CenterX = 0,
                CenterY = 0
            };
            var scaleTransform = new ScaleTransform
            {
                ScaleX = scale,
                ScaleY = scale
            };
            var translationTransform = new TranslateTransform
            {
                X = translateX,
                Y = translateY
            };
            // Create TransformGroup and add 3 transforms to it. 
            var transformGroup = new TransformGroup
            {
                Children = new TransformCollection { rotationTransform, scaleTransform, translationTransform }
            };
            return transformGroup;
        }

        // event handlers for user interaction

        private void OnScaleUpClicked(object sender, RoutedEventArgs e)
        {
            Scale *= 2;
        }

        private void OnScaleDownClicked(object sender, RoutedEventArgs e)
        {
            Scale /= 2;
        }
                
        private void OnMoveUpClicked(object sender, RoutedEventArgs e)
        {
            TranslateY -= 10;
        }

        private void OnMoveDownClicked(object sender, RoutedEventArgs e)
        {
            TranslateY += 10;
        }

        private void OnMoveLeftClicked(object sender, RoutedEventArgs e)
        {
            TranslateX -= 10;
        }
        private void OnMoveRightClicked(object sender, RoutedEventArgs e)
        {
            TranslateX += 10;
        }


    }
}
