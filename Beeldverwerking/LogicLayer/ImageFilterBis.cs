﻿using Globals;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace LogicLayer
{
    public class ImageFilterBis : IFilter
    {
        private readonly Dictionary<Globals.Filter, FilterOperation> filterOperations;
        public event Action<int> PartProcessed;

        public Bitmap OriginalImage { get; private set; }
        public Bitmap FilteredImage { get; private set; }


        public ImageFilterBis()
        {
            filterOperations = new Dictionary<Filter, FilterOperation>()
            {
                {
                    Filter.Original,
                    (p) => p
                },
                {
                    Filter.Negative,
                    (p) => Color.FromArgb(p.A, 255-p.R, 255-p.G, 255-p.B)
                },
                {
                    Filter.Red,
                    (p) => Color.FromArgb(p.A, p.R, 0, 0)
                },
                {
                    Filter.Green,
                    (p) => Color.FromArgb(p.A, 0, p.G, 0)
                },
                {
                    Filter.Blue,
                    (p) => Color.FromArgb(p.A, 0, 0,p.B)
                }
            };
            Load(@"Images\Turbo.jpg");
        }

        public void Load(string file)
        {
            
            OriginalImage = (Bitmap)Bitmap.FromFile(file);
            FilteredImage = new Bitmap(OriginalImage);
        }

        public void ApplyFilter(Filter filterMode)
        {
            ExecuteFilter(filterOperations[filterMode]);
        }

        public void ExecuteFilter(FilterOperation operation)
        {
            if (OriginalImage != null)
            {
                for (int i = 0; i < OriginalImage.Width; i++)
                {
                    for (int j = 0; j < OriginalImage.Height; j++)
                    {
                        FilteredImage.SetPixel(i, j, operation(OriginalImage.GetPixel(i, j)));
                    }
                    int percent = ((i + 1) * 100) / OriginalImage.Width;
                    PartProcessed?.Invoke(percent);
                }
            }
        }
    }
}
