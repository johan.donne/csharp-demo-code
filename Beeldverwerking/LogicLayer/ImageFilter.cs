﻿using Globals;
using System;
using System.Drawing;

namespace LogicLayer
{
    public class ImageFilter : IFilter
    {
        public event Action<int> PartProcessed;

        public Bitmap OriginalImage { get; private set; }
        public Bitmap FilteredImage { get; private set; }

        public ImageFilter()
        {
            Load(@"Images\Turbo.png");
        }

        public void Load(string file)
        {
            OriginalImage = (Bitmap)Bitmap.FromFile(file);
            FilteredImage = new Bitmap(OriginalImage);
        }

        public void ApplyFilter(Filter filterMode)
        {
            switch (filterMode)
            {
                case Filter.Original:
                    ExecuteFilter((p) => p);
                    break;
                case Filter.Negative:
                    ExecuteFilter((p) =>
                    {
                        return Color.FromArgb(p.A, 255 - p.R, 255 - p.G, 255 - p.B);
                    });
                    break;
                case Filter.Red:
                    ExecuteFilter((p) =>
                    {
                        return Color.FromArgb(p.A, p.R, 0, 0);
                    });
                    break;
                case Filter.Green:
                    ExecuteFilter((p) =>
                    {
                        return Color.FromArgb(p.A, 0, p.G, 0);
                    });
                    break;
                case Filter.Blue:
                    ExecuteFilter((p) =>
                    {
                        return Color.FromArgb(p.A, 0, 0, p.B);
                    });
                    break;

                default:
                    break;
            }
        }

        public void ExecuteFilter(FilterOperation operation)
        {
            if (OriginalImage != null)
            {
                for (int i = 0; i < OriginalImage.Width; i++)
                {
                    for (int j = 0; j < OriginalImage.Height; j++)
                    {
                        FilteredImage.SetPixel(i, j, operation(OriginalImage.GetPixel(i, j)));
                    }
                    int percent = ((i + 1) * 100) / OriginalImage.Width;
                    PartProcessed?.Invoke(percent);
                }
            }
        }

    }
}
