﻿using Globals;
using System;
using System.IO;
using System.Windows.Forms;

namespace FilterGUIMain
{
    public partial class MainForm : Form
    {
        private readonly IFilter filter;
        private bool loading = true;

        public MainForm(IFilter filter)
        {
            InitializeComponent();
            this.filter = filter;
            pictureBox.Image = filter.FilteredImage;
            filterComboBox.DataSource = Enum.GetValues(typeof(Filter));
            filterComboBox.SelectedItem = Filter.Original;
            filter.PartProcessed += Filter_PartProcessed;
            loading = false;
        }

        private void Filter_PartProcessed(int percentage)
        {
            progressBar.Value = percentage;
        }

        private void LoadButtonClick(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                InitialDirectory = Directory.GetCurrentDirectory() + @"\Images"
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filter.Load(dialog.FileName);
                pictureBox.Image = filter.FilteredImage;
                loading = true;
                filterComboBox.SelectedItem = Globals.Filter.Original;
                loading = false;
            }
        }

        private void FilterComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if (loading) return;
            progressBar.Value = 0;
            filter.ApplyFilter((Globals.Filter)filterComboBox.SelectedItem);
            pictureBox.Image = filter.FilteredImage;
            progressBar.Value = 0;
        }


    }
}
