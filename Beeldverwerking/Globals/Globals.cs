﻿using System.Drawing;

namespace Globals
{
    public enum Filter { Original, Negative, Red, Green, Blue }

    public delegate Color FilterOperation(Color pixel);
}
