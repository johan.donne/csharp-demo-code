﻿using System;
using System.Drawing;

namespace Globals
{
    public interface IFilter
    {
        event Action<int> PartProcessed;

        Bitmap OriginalImage { get; }
        Bitmap FilteredImage { get; }

        void Load(string file);
        void ApplyFilter(Filter filterMode);        
    }
}
