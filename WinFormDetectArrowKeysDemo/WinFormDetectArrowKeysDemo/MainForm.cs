﻿using System.Linq;
using System.Windows.Forms;

namespace WinFormDetectArrowKeysDemo
{
    public partial class MainForm : Form
    {
        // demo to intercept all keys (including SHIFT, CTRL, Arrow-Keys etc...
        //
        // Important: controls on te Form should have property 'TabStop' = False      



        public MainForm()
        {
            InitializeComponent();

            // make sure the form previews all Key input before it is passed to individual controls
            KeyPreview = true;
            // add special event-handler to mark the 'special' keys (Alt, Ctrl, Shift, arrow-keys...) as input-keys
            PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(OnMainFormPreviewKeyDown);
            // add handler for 'KeyDown' event
            KeyDown += OnMainFormKeyDown;
        }

        private void OnMainFormPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            // do not put other code here!
            e.IsInputKey = true;
        }

        private void OnMainFormKeyDown(object sender, KeyEventArgs e)
        {
            // handle key 
            var code = e.KeyCode;
            int value = e.KeyValue;
            var x = e.KeyData;

            var lines = resultTextBox.Lines.ToList();
            lines.Insert(0, $"code: {code.ToString()}, value: {value}, data: {x.ToString()}");
            resultTextBox.Lines = lines.ToArray();
        }


    }
}
