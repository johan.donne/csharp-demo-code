# C# Demo Code

Deze repository bevat een aantal voorbeeldtoepasingen  voor programmatie in c#.
Elke toepassing bevat naast de Visual Studio Solution ook een document met beschrijving van de werking en/of gebruikte technieken.

## Overzicht:

**Bitmaps - greyscale - raster** (WinForms)

Een toepassing voor manipulatie van een bitmap waarbij een ingelezen afbeelding kan omgezet worden naar grijswaarden en kan voorzien worden van een raster.  
Kernwoorden: *Bitmap, Grijswaarden, XNOR*

**Bitmaps - events - delegates ** (WinForms)

Een toepassing voor manipulatie van een bitmap waarbij een ingelezen afbeelding kan omgezet worden naar de kleurcomponenten en het negatieve beeld.  
Kernwoorden: *Bitmap, events, delegates, lambda expressions*


**Movie Ranking** (WinForms, class library)

Een toepassing voor het beheren van film-beoordelingen  
Kernwoorden:*CSV-bestand, Dictionary<T>, List<T>*

**Gravity** (WinForms,Class library)

Een toepassing met Physics simulatie van zwaartekracht en 1D-botsing die in Winforms geanimeerd weergegeven wordt
Kernwoord: *Lagenmodel, Simulatie, animatie, Bitmaps*

**WpfButtons** (WPF)

Een toepassing die demonstreert hoe je in WPF:

- vanuit code dynamisch knoppen kan toevoegen aan je GUI
- in de 'Click' eventhandler de gridpositie van een knop kan terugvinden
- properties van controls in je GUI vanuit code kan wijzigen/aanpassen

Kernwoorden: *Grid, Button, 'Click' event handler, dynamisch control aanmaken*  

**ContenControlDemo** (WPF)

Een toepassing die demonstreert hoe je in WPF dynamisch verschillende UserControls kan weergeven binnen je userInterface

Kernwoorden: *userinterface, dynamisch usercontrols aanmaken en tonen*


**WinFormDetectArrowKeysDemo** (WinForm)

Een demo van het onderscheppen van keys (ook de 'speciale' zoals Shift, Ctrl, Alt, arraow-keys...) in een WinForm toepassing.

Kernwoorden: *Key-down event, special keys*  

**Wpf Core 3 Bitmaps** (WPF)

Een toepassing die demonstreert hoe je in .Net Core 3.0 aan bitmanipulatie kan doen
in een afzonderlijke Classlibrary voor je Logische laag.
Dit kan gebruikt worden als alternatief voor een WinForm toepassing waarin het 'Bitmap' type van GDI+ gebruikt wordt.

Kernwoorden: *WPF, Bitmapmanipulatie, 3-lagen model*   

**PathFinder** (Console)

Een toepassing voor het zoeken van alle paden tussen twee punten in een ‘graaf’ (die bv. een
terrein in een computergame voorstelt).

Kernwoorden: *Recursie, Backtracking, Dictionary, List*   


**DirectoryTree** (Console)

Een console toepassing die een mappenstructuur van je schijf analyseert, en volgende informatie op
het scherm toont: ‘diepte’, aantal bestanden, aantal mappen, aantal gevonden extensies en de
(maximaal) 5 meest voorkomende extensies en het aantal keren dat ze voorkomen in de opgegeven
basismap.

Kernwoorden: *Recursie, Generic Collections, System.IO*   


**Wpf Transform canvas Content** (WPF)

Een toepassing die demonstreert hoe je in WPF de inhud van een 2D-canvas kan transformeren (verschuiven,
roteren, schalen) door de 'RenderTransform' van een te tekenen object te specifiëren.
Dit is een handige manier om een object te transformerenvan een lokaal (LCS) naar een 'wereld' coördinatensysteem (WCS).

Kernwoorden: *WPF, 2D rendering*  

**Audiotools** (WinForms)

Twee demo-programma's die het gebruik van de 'AudioTools' Nuget bibliotheek demonstreren.
Deze bibliotheek bevat klassen om op een eenvoudige manier toegang te krijgen tot de samples uit een willekeurig
audiobestand en deze (eventueel na bewerking) af te spelen.
De bibliotheek bevat ook nog twee hulpklassen: DelayLine & CircularBuffer.

Kernwoorden: *Digitale Audio, signaalverwerking*

**WpfMvvmMultiWindow** (Wpf)

Een demotoepassing met meerdere vensters die vanuit de toepassingslogica via Dependency Injection beheerd worden.

Kernwoorden: *Wpf, Multi-window, Dependency Injection, MVVM*     


