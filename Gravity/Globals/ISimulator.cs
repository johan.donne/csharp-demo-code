﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
    public interface ISimulator:IDisposable
    {

        int Height {  get; set;}
        float Elasticity { get; set;}
        void Start();
        void Stop();
      
        event Action<Ball> OnUiRefreshNeeded;
    }
}
