﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
    public struct Ball
    {
        private float radius;
        public float Position { get; set; }
        public float Speed { get; set; }
        public float Radius
        {
            get => radius;
            set => radius = value > 0 ? value : 0f;
        }

        public Ball(float position, float speed, float radius)
        {
            Position = position;
            Speed = speed;
            this.radius = radius;
        }


    }
}
