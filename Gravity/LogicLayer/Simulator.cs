﻿using Globals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;

namespace LogicLayer
{
    public class Simulator:ISimulator
    {
        private const int refreshInterval = 20;


        private Task worker;
        private CancellationTokenSource cancelSource;
        private Ball ball;
        private DateTime startTime;
        private float gravity = -0.5f;
        private float elasticity = 1.0f;

        public int Height { get; set; }
        public float Elasticity
        {
            get => elasticity;
            set
            {
                elasticity = value < 0 ? 0 : value > 1 ? 1 : value;
            }
        }

        public event Action<Ball> OnUiRefreshNeeded;

        public Simulator()
        {
            ball = new Ball(position: 0, speed: 0, radius: 20 );
        }

        public void Dispose()
        {
            Stop();
        }

        public void Start()
        {
            InitBall();
            var progress = new Progress<Ball>((b) => OnUiRefreshNeeded?.Invoke(b));
            cancelSource = new CancellationTokenSource();
            worker = Task.Run(() => DoWork(progress, cancelSource.Token), cancelSource.Token);
        }

        private void InitBall()
        {
            ball.Speed = 15;
            ball.Position = ball.Radius;
        }

        public void Stop()
        {
            cancelSource?.Cancel();
            worker?.Wait();
        }

        private void DoWork(IProgress<Ball> progress, CancellationToken token)
        {
            while(!token.IsCancellationRequested)
            {
                if((DateTime.Now - startTime).TotalMilliseconds >= refreshInterval)
                {
                    startTime = DateTime.Now;
                    UpdateBallPosition();
                    progress.Report(ball);
                }
                else
                {
                    Thread.Sleep(5);
                }
            }
        }
        

        private void UpdateBallPosition()
        {
            ball.Position += ball.Speed;
            if((ball.Position - ball.Radius*1.5 <= 0) && (ball.Speed < 0))
            {
                ball.Position = ball.Radius;
                ball.Speed =  -ball.Speed * Elasticity ;
            }
            else
            {
                ball.Speed += gravity;
            }
        }
    }
}
