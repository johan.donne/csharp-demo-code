﻿namespace BouncyGui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simulationBox = new System.Windows.Forms.PictureBox();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.elasticityTrackbar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.simulationBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elasticityTrackbar)).BeginInit();
            this.SuspendLayout();
            // 
            // simulationBox
            // 
            this.simulationBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.simulationBox.Location = new System.Drawing.Point(12, 29);
            this.simulationBox.Name = "simulationBox";
            this.simulationBox.Size = new System.Drawing.Size(160, 416);
            this.simulationBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.simulationBox.TabIndex = 0;
            this.simulationBox.TabStop = false;
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(222, 29);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 33);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            // 
            // stopButton
            // 
            this.stopButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopButton.Location = new System.Drawing.Point(222, 87);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 33);
            this.stopButton.TabIndex = 2;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            // 
            // elasticityTrackbar
            // 
            this.elasticityTrackbar.LargeChange = 1;
            this.elasticityTrackbar.Location = new System.Drawing.Point(232, 182);
            this.elasticityTrackbar.Name = "elasticityTrackbar";
            this.elasticityTrackbar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.elasticityTrackbar.Size = new System.Drawing.Size(56, 194);
            this.elasticityTrackbar.TabIndex = 3;
            this.elasticityTrackbar.TickFrequency = 5;
            this.elasticityTrackbar.Value = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(219, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Elasticity";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 484);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.elasticityTrackbar);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.simulationBox);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.Text = "Gravity";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.simulationBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elasticityTrackbar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox simulationBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.TrackBar elasticityTrackbar;
        private System.Windows.Forms.Label label1;
    }
}

