﻿using Globals;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BouncyGui
{
    public partial class MainForm:Form
    {
        private readonly ISimulator simulator;
        private readonly Brush brush = new SolidBrush(Color.DarkBlue);

        public MainForm(ISimulator simulator)
        {
            this.simulator = simulator;
            InitializeComponent();
            //DoubleBuffered = true;
            simulationBox.Image = new Bitmap(simulationBox.Width, simulationBox.Height);
            simulationBox.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
            simulator.Height = simulationBox.Height;
            this.FormClosing += MainFormClosing;
            simulator.OnUiRefreshNeeded += Simulator_OnUiRefreshNeeded;
            simulationBox.SizeChanged += SimulationBoxSizeChanged;
            elasticityTrackbar.ValueChanged += ElasticityTrackbar_ValueChanged;
            startButton.Click += StartButtonClick;
            stopButton.Click += StopButtonClick;

        }

        private void ElasticityTrackbar_ValueChanged(object sender, EventArgs e)
        {
            simulator.Elasticity = elasticityTrackbar.Value/10f;
        }

        private void Simulator_OnUiRefreshNeeded(Ball ball)
        {
            Redraw(ball);
        }

       

        private void Redraw(Ball ball)
        {
            using(var g = Graphics.FromImage(simulationBox.Image))
            {
                g.Clear(Color.White);
                g.FillEllipse(brush, simulationBox.Width / 2 - ball.Radius, simulationBox.Height - ball.Position - ball.Radius, ball.Radius * 2, ball.Radius * 2);
            };
            simulationBox.Invalidate(true);
        }

        private void MainFormClosing(object sender, FormClosingEventArgs e)
        {
            simulator.OnUiRefreshNeeded -= Simulator_OnUiRefreshNeeded;
            simulator.Dispose();
            brush.Dispose();
        }

        private void StopButtonClick(object sender, EventArgs e)
        {
            simulator.Stop();
            startButton.Enabled = true;
            stopButton.Enabled = false;
        }

        private void StartButtonClick(object sender, EventArgs e)
        {
            simulator.Start(); ;
            startButton.Enabled = false;
            stopButton.Enabled = true;
        }

        private void SimulationBoxSizeChanged(object sender, EventArgs e)
        {
            simulationBox.Image?.Dispose();
            simulationBox.Image = new Bitmap(simulationBox.Width, simulationBox.Height);
            simulator.Height = simulationBox.Width;
        }
                
    }
}
