﻿using System;

namespace Globals
{
    public class MovieScore : IComparable<MovieScore>
    {

        public string Name { get; }
        public int TotalScore { get; private set; }
        public int NrOfVotes { get; private set; }

        public double AverageScore
        {
            get
            {
                return ((double)TotalScore) / NrOfVotes;
            }
        }

        // expression bodied version 
        //public double AverageScore =>  ((double)TotalScore) / NrOfVotes;



        public Occurence Popularity
        {
            get
            {
                if (NrOfVotes >= 10) return Occurence.Popular;
                if (NrOfVotes >= 5) return Occurence.Common;
                return Occurence.Obscure;
            }
        }

        public MovieScore(string name, int score)
        {
            CheckScoreIsValid(score);
            Name = name;
            TotalScore = score;
            NrOfVotes = 1;
        }

        public void AddVote(int score)
        {
            CheckScoreIsValid(score);
            TotalScore += score;
            NrOfVotes++;
        }

        private void CheckScoreIsValid(int score)
        {
            if ((score < 0) || (score > 10))
                throw new ArgumentOutOfRangeException($"\"{score}\" is not a valid score.");
        }

        public int CompareTo(MovieScore other)
        {
            return TotalScore.CompareTo(other.TotalScore);
        }

        public override string ToString()
        {
            return $"{Name}({Popularity}) - {TotalScore}  - {NrOfVotes} votes - average: {AverageScore,5:F1}";
        }

    }
}
