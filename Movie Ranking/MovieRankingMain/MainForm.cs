﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Globals;

namespace MovieRankingMain
{
    public partial class MainForm : Form
    {
        private readonly IDataAccess dataAccessLayer;

        public MainForm(IDataAccess dal)
        {
            this.dataAccessLayer = dal;
            InitializeComponent();
        }

        private void EnterButtonClick(object sender, EventArgs e)
        {
            dataAccessLayer.AddMovieVote(nameTextBox.Text, int.Parse(scoretextBox.Text));
            RefreshResults();
        }

        private void RefreshResults()
        {
            var results = dataAccessLayer.SortedMovieList;
            var linesList = new List<string>();
            foreach (var result in results)
            {
                linesList.Add(result.ToString());
            }
            resultsTextBox.Lines = linesList.ToArray();
            
        }

        private void ImportButtonClick(object sender, EventArgs e)
        {
            dataAccessLayer.ImportVotes(@"resources\MovieVotes.csv");
            RefreshResults();
        }
    }
}
