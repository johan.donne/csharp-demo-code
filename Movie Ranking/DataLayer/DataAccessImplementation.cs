using System;
using System.Collections.Generic;
using System.IO;
using Globals;

namespace DataLayer
{
    public class DataAccessImplementation : IDataAccess
    {
        private readonly Dictionary<string, MovieScore> storage;
       
        public List<MovieScore> SortedMovieList
        {
            get

            {
                var result = new List<MovieScore>(storage.Values);
                result.Sort();
                result.Reverse();
                return result;
            }
        }

        public DataAccessImplementation()
        {
            storage = new Dictionary<string, MovieScore>();
        }

        public void AddMovieVote(string movieName, int score)
        {
            if (string.IsNullOrWhiteSpace(movieName)) return;
            movieName = movieName.Trim();
            string key = movieName.ToLower();
            if (storage.ContainsKey(key))
            {
                storage[key].AddVote(score);
            }
            else
            {
                storage.Add(key, new MovieScore(movieName, score));
            }
        }

        public void ImportVotes(string fileName)
        {
            string[] lines = File.ReadAllLines(fileName);
            foreach (string line in lines)
            {
                string[] elements = line.Split(',');
                AddMovieVote(elements[0].Trim().ToLower(), int.Parse(elements[1]));
            }
        }
    }
}
