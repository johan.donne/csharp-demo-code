﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BitmapLogic
{
    // Important: this project is of type 'Wpf Custom Control Class Library'


    public class Logic
    {
        private BitmapSource source;
        
        private int width;
        int height;
        int bytesPerPixel = 4;
        int stride;
        int totalPixels;
        byte[] pixels;

        public WriteableBitmap DisplaySource { get; set; }


        public Logic()
        {
            GetImage(@"resources\bitmapdemo.jpg");
            
        }

        public void GetImage(string path)
        {
            source = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));
            if (source.Format != PixelFormats.Bgra32) source = new FormatConvertedBitmap(source, PixelFormats.Bgra32, null, 0);
            width = source.PixelWidth;
            height = source.PixelHeight;
            bytesPerPixel = 4;
            stride = width * bytesPerPixel;
            totalPixels = width * height * bytesPerPixel;
            pixels = new byte[totalPixels];
            DisplaySource = new WriteableBitmap(source);
        }

        
        public void SetOriginal()
        {
           source.CopyPixels(pixels, stride, 0);
           var rectangle = new Int32Rect(0, 0, width, height);
           DisplaySource.WritePixels(rectangle, pixels, stride, 0, 0);
        }

        public void SetInverted()
        {
            DisplaySource.CopyPixels(pixels, stride, 0);
            for (int i = 0; i < totalPixels; i++)
            {
                // every fourth byte is an alpha value. just leave it
                if ((i + 1) % 4 != 0)
                {
                    pixels[i] = (byte)(255 - pixels[i]);
                }
            }

            var rectangle = new Int32Rect(0, 0, width, height);
            DisplaySource.WritePixels(rectangle, pixels, stride, 0, 0);
        }

    }
}
