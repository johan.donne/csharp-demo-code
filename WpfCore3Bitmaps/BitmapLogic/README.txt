﻿Belangrijk:

In een 'gewone' Class library voor .Net Core is het niet mogelijk te werken met Bitmaps ben andere media-elementen.
Om dit toch mogelijk te maken, ga je als volgt tewerk:


•	Je voegt aan je solution een nieuw project toe van het type ‘WPF Custom Control Library (.Net Core)’.
•	Daarin wis je ‘CustomControl1.cs’ en de ‘Themes’ map.
•	Daarin voeg je een nieuwe klasse toe (je logica-klasse)

Nu heb je een class library waarin je alle namespaces voor WPF beschikbaar hebt (en dus ook die voor bitmapbewerkingen).
