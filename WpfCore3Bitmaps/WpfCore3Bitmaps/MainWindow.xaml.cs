﻿using BitmapLogic;
using Microsoft.Win32;
using System.Windows;

namespace WpfCore3Bitmaps
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Logic logic;

        public MainWindow()
        {
            InitializeComponent();
            //create backend & connect it to imageBox
            logic = new Logic();
            ConnectImage();
        }

        private void ConnectImage()
        {
            imageBox.Source = logic.DisplaySource;
            infotextBlock.Text = $"{logic.DisplaySource.PixelHeight} x {logic.DisplaySource.PixelWidth}";
        }

        private void OnOriginalButtonClick(object sender, RoutedEventArgs e)
        {
            logic.SetOriginal();
        }

        private void OnInvertedButtonClick(object sender, RoutedEventArgs e)
        {
            logic.SetInverted();
        }

        private void OnSelectButtonClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog { InitialDirectory = @"resources" };
            bool? result = dialog.ShowDialog();
            if (result ?? false)
            {
                logic.GetImage(dialog.FileName);
            }
            ConnectImage();
        }
    }
}
