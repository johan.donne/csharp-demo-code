﻿using Globals.enums;

namespace Globals.Interfaces;

public interface IWindowManager
{
    void ShowWindow(ViewType viewType, bool isModal = false);
}
