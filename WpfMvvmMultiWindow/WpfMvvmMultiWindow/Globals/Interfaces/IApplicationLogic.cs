﻿namespace Globals.Interfaces;

public interface IApplicationLogic
{
    void ShowMainWindow();
    void ShowStartWindow();
}
