﻿using Globals.enums;
using Globals.Interfaces;

namespace LogicLayer;
public class ApplicationLogic : IApplicationLogic
{

    private readonly IWindowManager _windowManager;

    public ApplicationLogic(IWindowManager windowManager)
    {
        _windowManager = windowManager;
    }

    public void ShowMainWindow()
    {
        _windowManager.ShowWindow(ViewType.Main);
    }

    public void ShowStartWindow()
    {
        _windowManager.ShowWindow(ViewType.Start);
    }
}

