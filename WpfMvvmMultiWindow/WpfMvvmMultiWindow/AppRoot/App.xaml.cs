﻿using Globals.Interfaces;
using LogicLayer;
using Microsoft.Extensions.DependencyInjection;
using PresentationLayer;
using System.Windows;

namespace AppRoot;

/// <summary>
/// Composition root for application
/// </summary>
public partial class App : Application
{
    private readonly IServiceProvider _serviceProvider;

    public App()
    {
        var serviceCollection = new ServiceCollection();
        ConfigureServices(serviceCollection);
        _serviceProvider = serviceCollection.BuildServiceProvider();
    }

    private void ConfigureServices(ServiceCollection serviceCollection)
    {
        serviceCollection.AddLogicServices();
        serviceCollection.AddPresentationServices();
    }

    protected override void OnStartup(StartupEventArgs e)
    {
        var logic = _serviceProvider.GetService<IApplicationLogic>();
        logic?.ShowStartWindow();
    }
}
