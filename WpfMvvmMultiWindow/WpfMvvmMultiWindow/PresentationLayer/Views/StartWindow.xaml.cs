﻿using PresentationLayer.ViewModels;
using System.Windows;

namespace PresentationLayer.Views;
/// <summary>
/// Interaction logic for StartWindow.xaml
/// </summary>
public partial class StartWindow : Window
{
    private readonly StartViewModel _viewModel;

    public StartWindow(StartViewModel viewModel)
    {
        _viewModel = viewModel;
        DataContext = _viewModel;
        InitializeComponent();
        _viewModel.ShouldClose += OnClose;
    }

    private void OnClose()
    {
        Close(); ;
    }
}
