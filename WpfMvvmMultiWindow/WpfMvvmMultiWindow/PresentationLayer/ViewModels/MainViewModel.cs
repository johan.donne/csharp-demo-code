﻿using Globals.Interfaces;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System;

namespace PresentationLayer.ViewModels;
public class MainViewModel : ObservableObject
{
    private readonly IApplicationLogic _logic;

    private string _title = "WpfApp (MVVM)";

    public event Action? ShouldClose;

    // binding properties

    public string Title
    {
        get => _title;
        private set => SetProperty<string>(ref _title, value);
    }

    public string Time => DateTime.Now.ToLongTimeString();

    public IRelayCommand UpdateCommand { get; }
    public IRelayCommand CloseCommand { get; }

    public MainViewModel(IApplicationLogic logic)
    {
        _logic = logic;
        UpdateCommand = new RelayCommand(NotifyTimeChanged);
        CloseCommand = new RelayCommand(ShowStartWindow);
    }

    private void NotifyTimeChanged()
    {
        OnPropertyChanged(nameof(Time));
    }

    private void ShowStartWindow()
    {
        _logic.ShowStartWindow();
        ShouldClose?.Invoke();
    }
}
