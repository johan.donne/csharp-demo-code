﻿using Globals.Interfaces;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System;

namespace PresentationLayer.ViewModels;

public class StartViewModel : ObservableObject
{

    private readonly IApplicationLogic _logic;

    public event Action? ShouldClose;

    public IRelayCommand StartCommand { get; }

    public StartViewModel(IApplicationLogic logic)
    {
        _logic = logic;
        StartCommand = new RelayCommand(Start);
    }

    private void Start()
    {
        _logic.ShowMainWindow();
        ShouldClose?.Invoke();
    }
}
