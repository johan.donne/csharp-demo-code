﻿using Globals.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using PresentationLayer.Services;
using PresentationLayer.ViewModels;
using PresentationLayer.Views;

namespace PresentationLayer;
public static class ServiceExtensions
{

    public static void AddPresentationServices(this ServiceCollection services)
    {
        // Register the classes that need to be injected as singleton or transient (or scoped).

        services.AddTransient<MainWindow>();
        services.AddTransient<MainViewModel>();
        services.AddTransient<StartWindow>();
        services.AddTransient<StartViewModel>();
        services.AddSingleton<IWindowManager, WindowManager>();
    }
}