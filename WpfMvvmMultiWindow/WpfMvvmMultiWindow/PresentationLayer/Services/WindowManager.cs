﻿using Globals.enums;
using Globals.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using PresentationLayer.Views;
using System;
using System.Windows;

namespace PresentationLayer.Services;

public class WindowManager : IWindowManager
{
    private readonly IServiceProvider _serviceProvider;

  
    public WindowManager(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public void ShowWindow(ViewType viewType, bool isModal = false)
    {
        Window? window = viewType switch
        {
            ViewType.Main => _serviceProvider.GetService<MainWindow>(),
            ViewType.Start => _serviceProvider.GetService<StartWindow>(),
            _ => null,
        };
        if (isModal)
        {
            window?.ShowDialog();
        }
        else
        {
            window?.Show();
        }
    }
}
