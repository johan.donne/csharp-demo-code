﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AudioTools;
using System.IO;

namespace AudioPlayerDemo
{
    public partial class MainForm:Form
    {
        private readonly AudioLogic logic;
        private string selectedDirectory;
        private bool playing;
        private bool sourcePresent;
        private readonly Timer uiTimer;

        
        private bool Playing
        {
            get => playing;
            set
            {
                playing = value;
                SetButtons(playing);
            }
        }
        private bool SourcePresent
        {
            get => sourcePresent;
            set
            {
                sourcePresent = value;
                SetButtons(playing);
            }
        }

        public MainForm()
        {
            InitializeComponent();
            SourcePresent = false;
            Playing = false;
            logic = new AudioLogic();
            uiTimer = new Timer { Interval = 200 };
            uiTimer.Tick += UiTimerTick;
        }

        private void UiTimerTick(object sender, EventArgs e)
        {
           if  (Playing)
            {
                postionTextBox.Text = $"{logic.AudioPosition:hh\\:mm\\:ss}";
                if (logic.AudioPosition > logic.AudioLength)
                {
                    logic.Reset();
                    SourcePresent = false;
                }
            };
        }

        private void SetButtons(bool playing)
        {
            stopButton.Enabled = playing&&SourcePresent;
            volumeBar.Enabled = playing && SourcePresent;
            startButton.Enabled = !playing&&SourcePresent;
        }


        private void StopButtonClick(object sender, EventArgs e)
        {
            Stop();
        }

        private void Stop()
        {
            logic.Stop();
            Playing = false;
            uiTimer.Stop();
            uiTimer.Dispose();
        }

        private void SelectButtonClick(object sender, EventArgs e)
        {
            using(var dialog = new OpenFileDialog())
            {
                if(!string.IsNullOrWhiteSpace(selectedDirectory)) dialog.InitialDirectory = selectedDirectory;
                dialog.Filter = "geluidsbestanden|*.mp3;*.wav|Alle bestanden (*.*)|*.*";
                if(dialog.ShowDialog() == DialogResult.OK)
                {
                    Play(dialog);
                }
            }
        }

        private void Play(OpenFileDialog dialog)
        {
            logic.Reset();
            Playing = false;
            selectedDirectory = Path.GetDirectoryName(dialog.FileName);
            string filename = Path.GetFileName(dialog.FileName);
            fileNameTextBox.Text = Path.GetFileName(dialog.FileName);
            try
            {
                logic.Init(dialog.FileName);
                durationTextBox.Text = $"{logic.AudioLength:hh\\:mm\\:ss}";
                SourcePresent = true;
            }
            catch (ArgumentException exception)
            {
                fileNameTextBox.Text = $"{filename}: {exception.Message}";
            }
        }

        private void StartButtonClick(object sender, EventArgs e)
        {
            logic.Start();
            Playing = true;
            uiTimer.Start();
        }
               
        private void MainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            uiTimer.Stop();
            uiTimer.Dispose();
            logic.Reset();            
        }

        private void VolumeBarValueChanged(object sender, EventArgs e)
        {
            logic.Volume = volumeBar.Value;
        }
    }
}
