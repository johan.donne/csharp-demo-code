﻿using AudioTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioPlayerDemo
{
    public class AudioLogic
    {
        private AudioReader reader;
        private IAudioPlayer player;

        public TimeSpan AudioLength => reader?.TimeLength ?? new TimeSpan();
        public TimeSpan AudioPosition => reader?.TimePosition ?? new TimeSpan();
        public int Volume
        {
            get => (int)reader.Volume * 100;
            set
            {
                if(value < 0) reader.Volume = 0F;
                if(value > 100) reader.Volume = 1F;
                reader.Volume = value / 100F;
            }
        }

        public void Init(string fileName)
        {
            reader = new AudioReader(fileName);
            player = new AudioPlayer(reader.SampleRate);
            player.OnSampleFramesNeeded += Player_OnSampleFramesNeeded;
        }

        private void Player_OnSampleFramesNeeded(int frameCount)
        {
            for(int i = 0; i < frameCount; i++)
            {
                player.WriteSampleFrame(reader.ReadSampleFrame());
            }
        }

        public void Reset()
        {
            Stop();
            player?.Dispose();
            reader?.Dispose();
        }

        public void Stop()
        {
            player?.Stop();
        }

        public void Start()
        {
            player?.Start();
        }
    }
}
