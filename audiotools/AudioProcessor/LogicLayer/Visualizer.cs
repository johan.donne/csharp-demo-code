﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AudioTools;


namespace LogicLayer
{
    public class Visualizer
    {
        private const int displaySize = 1000; // approx 1000 x 1 msec = 3sec.
        private ConcurrentQueue<float> sampleQueue;
        private readonly Task worker;
        private TimeSpan workerInterval;
        private readonly CancellationTokenSource cancelSource;
        private bool working;
       
        public event Action OnUpdateNeeded;
        public CircularBuffer<float> MinQueue { get; }
        public CircularBuffer<float> MaxQueue { get; }


        public int SampleRate { get; set; }

        public Visualizer(TimeSpan interval)
        {
            MinQueue = new CircularBuffer<float>(displaySize);
            MaxQueue = new CircularBuffer<float>(displaySize);
            workerInterval = interval;
            cancelSource = new CancellationTokenSource();
            working = false;
            SampleRate = 44100;
            worker = Task.Run(() => { DoWork(cancelSource.Token); });
        }

        public void Start(ConcurrentQueue<float> sampleQueue)
        {
            this.sampleQueue = sampleQueue;
            working = true;
        }

        public void Stop()
        {
            working = false;
        }

        public void Dispose()
        {
            cancelSource.Cancel();
            worker.Wait();
        }

        private void DoWork(CancellationToken token)
        {
            while(!token.IsCancellationRequested)
            {
                if(working)
                {
                    var samples = GetSamplesFromQueue();
                    if (samples.Count>0) GetMinMax(samples);
                    OnUpdateNeeded?.Invoke();
                }
                Task.Delay(workerInterval).Wait();
            }
        }

        private void GetMinMax(List<float> samples)
        {
            const int blockTime = 3; // in msec
            int blocksize = SampleRate * blockTime / 1000;
            int fullBlockCount = samples.Count / blocksize;
            for(int i = 0; i < fullBlockCount; i++)
            {
                var block = samples.GetRange(i * blocksize, blocksize);
                GetBlockMinMax(block);
            }
            if(samples.Count <= fullBlockCount * blocksize) return;
            var finalBlock = samples.GetRange(fullBlockCount * blocksize, samples.Count- (fullBlockCount * blocksize));
            GetBlockMinMax(finalBlock);
        }

        private void GetBlockMinMax(List<float> block)
        {
            MinQueue.Add(block.Min());
            MaxQueue.Add(block.Max());
        }

        private List<float> GetSamplesFromQueue()
        {
            var samples = new List<float>();
            while(sampleQueue.TryDequeue(out float sample))
            {
                samples.Add(sample);
            }
            return samples;
        }
    }
}
