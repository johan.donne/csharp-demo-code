﻿using AudioTools;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer
{
    internal class AudioProcessor
    {
        private readonly IAudioReader reader;
        private readonly IAudioPlayer player;
        private readonly DelayLine<AudioSampleFrame> delayLine;
        private static TimeSpan maxDelay = TimeSpan.FromMilliseconds(1000);
        private TimeSpan delay = TimeSpan.FromMilliseconds(0);
        private readonly ConcurrentQueue<float> visualisationQueue;

        public static TimeSpan MaxDelayTime => maxDelay;
        public TimeSpan DelayTime
        {
            get => TimeSpan.FromMilliseconds(delayLine.Delay * 1000 / reader.SampleRate);
            set
            {
                delay = (value <= MaxDelayTime) ? value : MaxDelayTime;
                if(delayLine != null) delayLine.Delay = TimeSpanToFrames(delay);
            }
        }

        public AudioProcessor(IAudioReader reader, IAudioPlayer player, ConcurrentQueue<float> visualisationQueue)
        {
            this.reader = reader;
            this.player = player;
            this.visualisationQueue = visualisationQueue;
            delayLine = new DelayLine<AudioSampleFrame>(TimeSpanToFrames(MaxDelayTime));
            player.OnSampleFramesNeeded += OnPlayerSampleFramesNeeded;
        }

        private int TimeSpanToFrames(TimeSpan interval)
        {
            return (int)interval.TotalMilliseconds * (reader?.SampleRate ?? 0) / 1000;
        }

        
        #region player backgroundthread

        private void OnPlayerSampleFramesNeeded(int frameCount)
        {
            // this event is called on an background thread
            for (int i = 0; i < frameCount; i++)
            {
                var frame = CalculateNextFrame();
                visualisationQueue?.Enqueue(frame.Left);
                player.WriteSampleFrame(frame);
            }
        }

        private AudioSampleFrame CalculateNextFrame()
        {
            var frame = reader.ReadSampleFrame();
            delayLine.Enqueue(frame);
            return frame + delayLine.Dequeue().Amplify(0.7F);
        }

        //private AudioSampleFrame CalculateNextFrame()
        //{
        //    // generate reverb instead of echo
        //    var frame = reader.ReadSampleFrame();
        //    var output = frame + delayLine.Dequeue();
        //    delayLine.Enqueue(frame = output.Amplify(0.7F));
        //    return output;
        //}

        #endregion player backgroundthread


    }
}
