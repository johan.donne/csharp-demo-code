﻿using AudioTools;
using Globals;
using System;
using System.Collections.Concurrent;

namespace LogicLayer
{
    public class Logic : ILogic
    {

        private IAudioReader reader;
        private IAudioPlayer player;
        private AudioProcessor processor;
        private Visualizer visualizer;
        private float volume;
        private ConcurrentQueue<float> sampleQueue;
        private TimeSpan delay = TimeSpan.FromMilliseconds(0);

        public event Action OnEndOfAudio;
        public event Action OnUiUpdateNeeded;

        public CircularBuffer<float> MinQueue => visualizer?.MinQueue;
        public CircularBuffer<float> MaxQueue => visualizer?.MaxQueue;

        public TimeSpan AudioLength => reader?.TimeLength ?? new TimeSpan();
        public TimeSpan AudioPosition => reader?.TimePosition ?? new TimeSpan();

        public TimeSpan MaxDelayTime => AudioProcessor.MaxDelayTime;
        public TimeSpan DelayTime
        {
            get => delay;
            set
            {
                delay = (value <= MaxDelayTime) ? value : MaxDelayTime;
                if (processor == null) return;
                processor.DelayTime = value;
            }
        }

        public float Volume
        {
            get => volume;
            set
            {
                volume = value;
                if (value < 0) volume = 0F;
                if (value > 1) volume = 1F;
                if (reader != null) reader.Volume = volume;
            }
        }

        public void SelectSound(string filename)
        {
            if (visualizer == null) InitVisualizer();
            Reset();
            reader = new AudioReader(filename);
            Volume = volume;
            visualizer.SampleRate = reader.SampleRate;
            sampleQueue = new ConcurrentQueue<float>();
            player = new AudioPlayer(reader.SampleRate);

            processor = new AudioProcessor(reader, player, sampleQueue)
            {
                DelayTime = DelayTime
            };
        }


        private void InitVisualizer()
        {
            visualizer = new Visualizer(TimeSpan.FromMilliseconds(1));
            OnUiUpdateNeeded += () => CheckEndOfAudio();
            visualizer.OnUpdateNeeded += () => OnUiUpdateNeeded?.Invoke();
        }


        private void CheckEndOfAudio()
        {
            if (AudioPosition <= AudioLength) return;
            OnEndOfAudio?.Invoke();
            Stop();
        }

        public void Reset()
        {
            if (visualizer != null) visualizer.OnUpdateNeeded -= () => OnUiUpdateNeeded?.Invoke();
            Stop();
            player?.Dispose();
            player = null;
            reader?.Dispose();
            reader = null;
            MinQueue?.Clear();
            MaxQueue?.Clear();
        }

        public void Stop()
        {
            player?.Stop();
            visualizer?.Stop();
        }

        public void Start()
        {
            player?.Start();
            visualizer.Start(sampleQueue);
        }

        public void Dispose()
        {
            OnUiUpdateNeeded -= () => CheckEndOfAudio();
            Reset();
            visualizer?.Dispose();
        }



    }
}
