﻿using AudioTools;
using System;

namespace Globals
{
    public interface ILogic : IDisposable
    {
        event Action OnUiUpdateNeeded;
        event Action OnEndOfAudio;

        CircularBuffer<float> MinQueue { get; }
        CircularBuffer<float> MaxQueue { get; }

        TimeSpan AudioLength { get; }
        TimeSpan AudioPosition { get; }

        TimeSpan MaxDelayTime { get; }
        TimeSpan DelayTime { get; set; }
        float Volume { get; set; }

        void SelectSound(string filename);
        void Start();
        void Stop();
    }
}
