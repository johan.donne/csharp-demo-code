﻿namespace AudioProcessorMain
{ 
    
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectButton = new System.Windows.Forms.Button();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.durationTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.positionTextBox = new System.Windows.Forms.TextBox();
            this.volumeBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.delayTrackBar = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.delayTextBox = new System.Windows.Forms.TextBox();
            this.envelopePictureBox = new System.Windows.Forms.PictureBox();
            this.fpsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.volumeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.envelopePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // selectButton
            // 
            this.selectButton.Location = new System.Drawing.Point(420, 24);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(75, 32);
            this.selectButton.TabIndex = 0;
            this.selectButton.Text = "Select";
            this.selectButton.UseVisualStyleBackColor = true;
            this.selectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(26, 29);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.ReadOnly = true;
            this.fileNameTextBox.Size = new System.Drawing.Size(371, 22);
            this.fileNameTextBox.TabIndex = 1;
            this.fileNameTextBox.Text = "<select an audiofile>";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(420, 77);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 30);
            this.startButton.TabIndex = 2;
            this.startButton.Text = "Play";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(420, 117);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 30);
            this.stopButton.TabIndex = 3;
            this.stopButton.Text = "Pause";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Total time:";
            // 
            // durationTextBox
            // 
            this.durationTextBox.Location = new System.Drawing.Point(106, 77);
            this.durationTextBox.Name = "durationTextBox";
            this.durationTextBox.ReadOnly = true;
            this.durationTextBox.Size = new System.Drawing.Size(149, 22);
            this.durationTextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Position:";
            // 
            // positionTextBox
            // 
            this.positionTextBox.Location = new System.Drawing.Point(106, 117);
            this.positionTextBox.Name = "positionTextBox";
            this.positionTextBox.ReadOnly = true;
            this.positionTextBox.Size = new System.Drawing.Size(149, 22);
            this.positionTextBox.TabIndex = 6;
            // 
            // volumeBar
            // 
            this.volumeBar.Location = new System.Drawing.Point(281, 104);
            this.volumeBar.Maximum = 100;
            this.volumeBar.Name = "volumeBar";
            this.volumeBar.Size = new System.Drawing.Size(116, 56);
            this.volumeBar.SmallChange = 10;
            this.volumeBar.TabIndex = 7;
            this.volumeBar.TickFrequency = 10;
            this.volumeBar.Value = 50;
            this.volumeBar.ValueChanged += new System.EventHandler(this.OnVolumeBarValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(308, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Volume:";
            // 
            // delayTrackBar
            // 
            this.delayTrackBar.Location = new System.Drawing.Point(117, 170);
            this.delayTrackBar.Name = "delayTrackBar";
            this.delayTrackBar.Size = new System.Drawing.Size(378, 56);
            this.delayTrackBar.TabIndex = 8;
            this.delayTrackBar.ValueChanged += new System.EventHandler(this.OnDelayTrackBarValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Delay:";
            // 
            // delayTextBox
            // 
            this.delayTextBox.Location = new System.Drawing.Point(26, 190);
            this.delayTextBox.Name = "delayTextBox";
            this.delayTextBox.ReadOnly = true;
            this.delayTextBox.Size = new System.Drawing.Size(74, 22);
            this.delayTextBox.TabIndex = 9;
            // 
            // envelopePictureBox
            // 
            this.envelopePictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.envelopePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.envelopePictureBox.Location = new System.Drawing.Point(32, 240);
            this.envelopePictureBox.Name = "envelopePictureBox";
            this.envelopePictureBox.Size = new System.Drawing.Size(476, 176);
            this.envelopePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.envelopePictureBox.TabIndex = 10;
            this.envelopePictureBox.TabStop = false;
            // 
            // fpsLabel
            // 
            this.fpsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.fpsLabel.AutoSize = true;
            this.fpsLabel.Location = new System.Drawing.Point(34, 423);
            this.fpsLabel.Name = "fpsLabel";
            this.fpsLabel.Size = new System.Drawing.Size(0, 17);
            this.fpsLabel.TabIndex = 11;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 445);
            this.Controls.Add(this.fpsLabel);
            this.Controls.Add(this.envelopePictureBox);
            this.Controls.Add(this.delayTextBox);
            this.Controls.Add(this.delayTrackBar);
            this.Controls.Add(this.volumeBar);
            this.Controls.Add(this.positionTextBox);
            this.Controls.Add(this.durationTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.fileNameTextBox);
            this.Controls.Add(this.selectButton);
            this.Name = "MainForm";
            this.Text = "AudioProcessor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.volumeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.envelopePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectButton;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox durationTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox positionTextBox;
        private System.Windows.Forms.TrackBar volumeBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar delayTrackBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox delayTextBox;
        private System.Windows.Forms.PictureBox envelopePictureBox;
        private System.Windows.Forms.Label fpsLabel;
    }
}

