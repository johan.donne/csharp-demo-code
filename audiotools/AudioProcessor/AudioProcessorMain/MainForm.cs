﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Globals;
using System.Threading;

namespace AudioProcessorMain
{
    public partial class MainForm:Form
    {
        private const int envelopeWidth = 1000;

        private readonly ILogic logic;
        private readonly SynchronizationContext uiContext;

        private string selectedDirectory;
        private bool playing;
        private bool sourcePresent;
        private DateTime previousFrame;
        private int framecount;

        private bool Playing
        {
            get => playing;
            set
            {
                playing = value;
                SetControls();
            }
        }
        private bool SourcePresent
        {
            get => sourcePresent;
            set
            {
                sourcePresent = value;
                SetControls();
            }
        }

        public MainForm(ILogic logic)
        {
            InitializeComponent();
            this.logic = logic;
            uiContext = SynchronizationContext.Current;
            
            logic.OnUiUpdateNeeded += UpdateUi;
            logic.OnEndOfAudio += () => SourcePresent = false;
                       
            int envelopeHeight = envelopeWidth * envelopePictureBox.Height / envelopePictureBox.Width;
            envelopePictureBox.Image = new Bitmap(width: envelopeWidth, height: envelopeHeight);
            delayTrackBar.Maximum = (int)logic.MaxDelayTime.TotalMilliseconds;
            delayTrackBar.TickFrequency = delayTrackBar.Maximum / 20;

            SourcePresent = false;
            Playing = false;
            OnVolumeBarValueChanged(this, null);
            OnDelayTrackBarValueChanged(this, null);
        }

        private void UpdateUi()
        {
            uiContext.Post((dummy) =>
           {
               framecount++;
               if (framecount == 10)
               {
                   framecount = 0;
                   var now = DateTime.Now;
                   var delta = now - previousFrame;
                   previousFrame = now;
                   fpsLabel.Text = $"{10000F / delta.TotalMilliseconds:f0} fps";
                   fpsLabel.Refresh();
               }
               positionTextBox.Text = $"{logic.AudioPosition:hh\\:mm\\:ss}";
               positionTextBox.Refresh();
               DrawSoundEnvelope();
           }, null);
        }

        private void DrawSoundEnvelope()
        {
            float height = envelopePictureBox.Image.Height;
            float maxEnvelope = 0.9F * height / 2;
            using(var g = Graphics.FromImage(envelopePictureBox.Image))
            using(var pen = new Pen(Color.Black,1.3F))
            {
                g.Clear(Color.AntiqueWhite);
                for(int i = 0; i < logic.MinQueue.Length; i++)
                {
                    g.DrawLine(pen, i, TransformY(logic.MinQueue[i],maxEnvelope,height), i, TransformY(logic.MaxQueue[i], maxEnvelope,height));
                }
            }
            envelopePictureBox.Refresh();
        }

        private static float TransformY(float y, float maxY, float max)
        {
            return (max / 2) - (y * maxY);
        }

        private void SetControls()
        {
            stopButton.Enabled = Playing && SourcePresent;
            volumeBar.Enabled = SourcePresent;
            delayTrackBar.Enabled = SourcePresent;
            startButton.Enabled = !Playing && SourcePresent;
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            using(var dialog = new OpenFileDialog())
            {
                if(!string.IsNullOrWhiteSpace(selectedDirectory)) dialog.InitialDirectory = selectedDirectory;
                dialog.Filter = "geluidsbestanden|*.mp3;*.wav|Alle bestanden (*.*)|*.*";
                if(dialog.ShowDialog() == DialogResult.OK) SelectFile(dialog.FileName);
            }
        }

        private void SelectFile(string audiofile)
        {
            Playing = false;
            selectedDirectory = Path.GetDirectoryName(audiofile);
            string filename = Path.GetFileName(audiofile);
            fileNameTextBox.Text = Path.GetFileName(audiofile);
            try
            {
                logic.SelectSound(audiofile);
                durationTextBox.Text = $"{logic.AudioLength:hh\\:mm\\:ss}";
                SourcePresent = true;
            }
            catch(ArgumentException exception)
            {
                fileNameTextBox.Text = $"{filename}: {exception.Message}";
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            logic.Start();
            Playing = true;
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            logic.Stop();
            Playing = false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            logic.OnUiUpdateNeeded -= UpdateUi;
            logic.Dispose();
        }

        private void OnVolumeBarValueChanged(object sender, EventArgs e)
        {
            logic.Volume = volumeBar.Value / 100F; ;
        }

        private void OnDelayTrackBarValueChanged(object sender, EventArgs e)
        {
            logic.DelayTime = TimeSpan.FromMilliseconds(delayTrackBar.Value);
            delayTextBox.Text = $"{delayTrackBar.Value}ms";
        }

    }
}
